//
//  PhysicsObjectBoned.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/21/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "BonePhysicsObject.h"
@import GLKit;

@implementation BonePhysicsObject

-(GLKMatrix4)currentTransposition {
    return [_bone compoundTranslation];
}

-(void)setTransposition:(GLKMatrix4)transposition {
    self.bone.externalTransposition = transposition;
    [self.boundingBox updateBoxTransposition];
}

@end
