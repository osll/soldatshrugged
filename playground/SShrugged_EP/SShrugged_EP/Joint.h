//
//  Joint.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/21/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Bone.h"
@import GLKit;

@interface Joint : NSObject

@property (strong, nonatomic, readonly) NSString *name;
@property (strong, nonatomic) Bone * childBone;
@property (weak, nonatomic) Bone * parentBone;
@property (assign, nonatomic) GLKVector3 pointOfConnection;

-(id)initWithName:(NSString *)name
                parentBone:(Bone *)parentBone
                childBone:(Bone *)childBone
                connPoint:(GLKVector3)connPoint;

-(void)propogateTransposition:(GLKMatrix4)transposition;

-(float)rotationAngle;
-(void)rotateChildOverJoint:(float)angleZ;
-(void)setRotationConstrMinAng:(float)minAng maxRotAngle:(float)maxArg isFwrd:(BOOL)isFwd;

@end
