//
//  CollisionDetectionEngine.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/24/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SimObjFactory.h"

typedef void (^CollisionHandler)(SimulationObject *obj1, SimulationObject *obj2);

/* Object provides access point to knowledge on collision detection handling
 *
 * Responsibility:
 *   Store behaviors of objects in case of collisions
 *   Provide some data (e.g. diescarded objects) during CD session
 */
@interface CollisionResolver : NSObject

+(void)prepareForCheck;
+(NSMutableSet *)discardedTranslation;
+(NSMutableArray *)discardedObjectsIds;
+(void)resolveCollisionOfObject:(NSString *)obj1Id
                     withObject:(NSString *)obj2Id;

+(void)resolvePenetrationForObj:(SimulationObject *)obj1
                         andObj:(SimulationObject *)obj2;

@end
