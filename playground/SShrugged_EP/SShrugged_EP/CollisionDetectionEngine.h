//
//  CollisionAwareObjects.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/24/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CollisionDetectionEngine : NSObject

-(void)addObjectById:(NSString *)objectId;
-(void)removeObjectById:(NSString *)objectId;

-(void)processMovements;
-(BOOL)doesObjectCollidesWithSmth:(NSString *)objId;

@end
