//
//  Joint.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/21/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "Joint.h"

@implementation Joint {

    float _rotationAngle;
    float _minRotAngle;
    float _maxRotAngle;
    BOOL _isRotCnstntFwd;
}

-(id)initWithName:(NSString *)name
       parentBone:(Bone *)parentBone
        childBone:(Bone *)childBone
        connPoint:(GLKVector3)connPoint {
    self = [super init];
    if (self) {
        _name = name;
        _childBone = childBone;
        _parentBone = parentBone;
        _pointOfConnection = connPoint;
        
        _rotationAngle = 0;
        _minRotAngle = - M_PI;
        _maxRotAngle = M_PI;
        _isRotCnstntFwd = YES;
    }
    return self;
}

-(void)propogateTransposition:(GLKMatrix4)transposition {
    self.childBone.externalTransposition = transposition;
}

-(BOOL)doesRotConstraintsHold:(float)newAngle {
    return !((_minRotAngle <= newAngle && newAngle <= _maxRotAngle) ^ _isRotCnstntFwd);
}

-(void)setRotationConstrMinAng:(float)minAng maxRotAngle:(float)maxArg isFwrd:(BOOL)isFwd {
    _minRotAngle = minAng;
    _maxRotAngle = maxArg;
    
    _isRotCnstntFwd = isFwd;
}

-(float)rotationAngle { return _rotationAngle; }

-(void)rotateChildOverJoint:(float)angleZ {
    float newAngle = _rotationAngle + angleZ;
    while (newAngle < (-M_PI - FLT_EPSILON)) { newAngle += 2 * M_PI; }
    while (newAngle > (M_PI + FLT_EPSILON)) { newAngle -= 2 * M_PI; }
    
    if (![self doesRotConstraintsHold:newAngle]) {    return;
    }

    _rotationAngle = newAngle;
    
    
    GLKMatrix4 jointoRorationMtx = GLKMatrix4MakeTranslation(_pointOfConnection.x, _pointOfConnection.y, _pointOfConnection.z);
    jointoRorationMtx = GLKMatrix4RotateZ(jointoRorationMtx, angleZ);
    jointoRorationMtx = GLKMatrix4Translate(jointoRorationMtx, -_pointOfConnection.x, -_pointOfConnection.y, -_pointOfConnection.z);
    
    self.childBone.selfTransposition = GLKMatrix4Multiply(self.childBone.selfTransposition, jointoRorationMtx);

}

@end
