//
//  Command.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/20/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Command <NSObject>

@required
-(BOOL)performCommand;

@end
