//
//  MeshComposite.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/18/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "Mesh.h"

@interface MeshComposite : Mesh

-(id)initWithMeshes:(NSArray *)meshes;
-(void)draw:(GLKMatrix4)meshTransform;

@end
