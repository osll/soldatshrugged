//
//  CompositeCollisionBox.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/19/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CollisionBox.h"

@interface CompositeCollisionBox : CollisionBox

-(id)initWithChildren:(NSArray *)parts;
-(BOOL)checkCollision:(CollisionBox *)actor;
-(NSArray *)parts;

-(float)maxX;
-(float)minX;

@end
