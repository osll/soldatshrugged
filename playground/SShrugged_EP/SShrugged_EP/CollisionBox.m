//
//  CollisionBox.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/19/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "CollisionBox.h"

@implementation CollisionBox

-(id)init {
    return [self initWithTranslation:GLKMatrix4Identity];
}

//designated
-(id)initWithTranslation:(GLKMatrix4)initTranslation {
    self = [super init];
    if (self) {
        _selfTransposition = initTranslation;
    }
    return self;
}

-(BOOL)checkCollision:(CollisionBox *)actor{
    NSAssert(false, @"Should be implemented by Subclasses");
    return NO;
}

-(BOOL)checkIfPosInside:(GLKVector4)pos {
    NSAssert(false, @"Should be implemented by Subclasses");
    return NO;
}

-(float)maxX {
    NSAssert(false, @"Should be implemented by Subclasses");
    return 0;
}
-(float)minX {
    NSAssert(false, @"Should be implemented by Subclasses");
    return 0;
}

-(float)maxY {
    NSAssert(false, @"Should be implemented by Subclasses");
    return 0;
}
-(float)minY {
    NSAssert(false, @"Should be implemented by Subclasses");
    return 0;
}

-(GLKMatrix4)boxTransposition {
    if (_transpositionProvider) {
        return GLKMatrix4Multiply([self.transpositionProvider currentTransposition], self.selfTransposition);
    }
    
    return self.selfTransposition;
}

-(void)updateBoxTransposition {
    NSAssert(false, @"Should be implemented by Subclasses");
}

@end
