//
//  Bone.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/21/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
@import GLKit;

@interface Bone : NSObject

@property (strong, readonly, nonatomic) NSMutableSet *childJoints;

@property (assign, nonatomic) GLKMatrix4 selfTransposition;
@property (assign, nonatomic) GLKMatrix4 externalTransposition;

-(id)init;
-(GLKMatrix4)compoundTranslation;

@end
