//
//  GameManager.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/20/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "GameManager.h"
#import "MeshComposite.h"
#import "PhysicsManager.h"
#import "WorldCamera.h"
#import "SimulationInfo.h"

#import "AnimationManager.h"

#import "BonePhysicsObject.h"
#import "InputManager.h"
#import "Weapon.h"
#import "WarriorGameStrategy.h"

@interface GameManager ()
@property (assign, nonatomic) float lastShootElapsedTime;
@end

@implementation GameManager {
    uint _bulletCntr;
}

-(id)init {
    self = [super init];
    if (self) {
    }
    return self;
}

-(void)update {
    _lastShootElapsedTime += [PhysicsManager timeDelta];
}

+(void)update {
    [[GameManager instance] update];
    [WorldCamera update:[PhysicsManager timeDelta]];
}

+(GameManager *)instance {
    static GameManager *inst;
    if (!inst) { inst = [GameManager new]; }
    return inst;
}

-(NSString *)generateBulletName {
    return [NSString stringWithFormat:@"bullet%d", ++_bulletCntr];
}

-(void)fire:(SimulationObject *)emitter {
    SimulationObject *player = [SimulationInfo player];
    id<Weapon> currWpn = ((WarriorGameStrategy*)player.gameSt).currWeapon;
    GLKVector3 recoil = GLKVector3Make(0, 0, 0);
    NSArray *bullets = [currWpn createBulletWithId:[self generateBulletName]];
    for (SimulationObject *bullet in bullets) {
    
        [emitter setupParticlePhysicsParams:bullet];
        [PhysicsManager addAlwaysMovingObject:bullet.name];
    
        [InputManager recoil:player.name];
    }
    
    [currWpn postprocessShoot:recoil];
    _lastShootElapsedTime = 0;
}

+(void)fire:(NSString *)emitterName {
    GameManager *gm = [GameManager instance];
    if (gm.lastShootElapsedTime < gm.playerFireRate) {
        return;
    }
    
    [gm fire:[SimObjFactory createdObjectById:emitterName]];
}

+(void)setupWorld {
    [SimObjFactory createObjectOfType:@"static_map" withId:@"static_map"];
    SimulationObject *hf = [SimObjFactory createObjectOfType:@"warrior" withId:@"HatlessFox"];
    hf.desiredWorldTransformMtx = GLKMatrix4Translate(hf.worldTransformMtx, 0, 2.0, 0);
  //  [((WGS*)hf.gameSt) addWeapon:@"ak"];
    [((WGS*)hf.gameSt) addWeapon:@"knife"];
    /*
    SimulationObject *enemy = [SimObjFactory createObjectOfType:@"warrior"
                                                         withId:@"enemy1"];
    enemy.desiredWorldTransformMtx = GLKMatrix4Translate(
        enemy.worldTransformMtx, -2.5, 4, 0);
*/
}

+(void)prepareForStart {
    [WorldCamera instance].zoomFactor = 6.0;
    [WorldCamera moveDepth:-20.0f];
    [SimulationInfo setControlledPlayerName:@"HatlessFox"];
}

+(void)jump:(NSString *)objName {
    SimulationObject *obj = [SimObjFactory createdObjectById:objName];
    obj.physicsSt.currVelocity = GLKVector3Make(0, 0.15, 0);
    [AnimationManager addAnimationId:@"WarriorJumping" byKey:objName forTarget:[SimulationInfo controlledPlayerName]];
}

+(void)turnAround:(NSString *)objectId {
    SimulationObject *so = [SimObjFactory createdObjectById:objectId];
    PhysicsObject *simObjPO = so.physicsSt.physObject;
    PhysicsObject *controllerPO = [so.physicsSt.physObject controllerPart];
    float dx = (controllerPO.boundingBox.maxX - controllerPO.boundingBox.minX) / 2;
    
    //move to bb middle
    GLKMatrix4 rotation = GLKMatrix4MakeTranslation(dx, 0.0f, 10);
    rotation = GLKMatrix4RotateY(rotation, M_PI);
    
    //FIXME: generalize/move to super
    if ([controllerPO isKindOfClass:[BonePhysicsObject class]]) {
        Bone *bn = ((BonePhysicsObject *)controllerPO).bone;
        bn.selfTransposition = GLKMatrix4Multiply(bn.selfTransposition, rotation);
    
        //simulate "do nothing" with bb
        bool isInv;
        GLKMatrix4 rotationInv = GLKMatrix4Invert(rotation, &isInv);
        simObjPO.boundingBox.selfTransposition = GLKMatrix4Multiply(
            rotationInv, simObjPO.boundingBox.selfTransposition);
    }
    
}

-(float)playerFireRate {
    return [((WGS*)[SimulationInfo player].gameSt).currWeapon fireRate];
}

+(float)slowDownFactor {
    return 1;
}

@end
