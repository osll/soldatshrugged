//
//  ObjectOrientedBoundingBox.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/31/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>
#import "RectangleCollisionBox.h"

@interface ObjectOrientedBoundingBox: RectangleCollisionBox

-(BOOL)checkCollision:(CollisionBox *)actor;

@end

typedef ObjectOrientedBoundingBox OOBB;