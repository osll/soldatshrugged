//
//  RectangleCollisionBox.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/19/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RectangleCollisionBox.h"

@interface AxisAlignedBoundingBox : RectangleCollisionBox



@end

typedef AxisAlignedBoundingBox AABB;