//
//  SimulationInfo.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/21/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SimObjFactory.h"

@interface SimulationInfo : NSObject

+(NSString *)controlledPlayerName;
+(void)setControlledPlayerName:(NSString *)name;
+(SimulationObject *)player;

@end
