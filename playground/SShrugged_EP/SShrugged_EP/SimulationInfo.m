//
//  SimulationInfo.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/21/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "SimulationInfo.h"
#import "WorldCamera.h"

@implementation SimulationInfo {
    NSString *_controlledPlayerName;
}

+(SimulationInfo *)instance {
    static SimulationInfo *inst;
    if (!inst) { inst = [SimulationInfo new]; }
    return inst;
}

-(NSString *)controlledPlayerName { return _controlledPlayerName; }
-(void)setControlledPlayerName:(NSString *)name { _controlledPlayerName = name;}

+(NSString *)controlledPlayerName {
    return [[self instance] controlledPlayerName];
}

+(void)setControlledPlayerName:(NSString *)name {
    [WorldCamera bindToObject:name];
    [[self instance] setControlledPlayerName:name];
}

+(SimulationObject *)player {
    return [SimObjFactory createdObjectById:[self controlledPlayerName]];
}

@end
