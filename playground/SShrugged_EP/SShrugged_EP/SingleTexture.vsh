//
//  SingleTexture.vsh
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/28/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

attribute vec3 a_position;
attribute vec2 a_textureCoord;

varying vec2 v_textureCoord;

uniform mat4 u_viewProjectionMatrix;

void main() {
    v_textureCoord = a_textureCoord;
    gl_Position = u_viewProjectionMatrix * vec4(a_position, 1.0);
}
