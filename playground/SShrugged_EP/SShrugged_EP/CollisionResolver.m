//
//  CollisionDetectionEngine.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/24/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "CollisionResolver.h"


#import "SkeletonKeyFrame.h"
#import "Animation.h"
#import "AnimationManager.h"
#import "SimulationInfo.h"
#import "WarriorGameStrategy.h"
#import "Knife.h"

@implementation CollisionResolver {
    NSMutableArray *_discardedObjIds;
    NSMutableDictionary *_collisionHandlers; //string to block mapping
    NSMutableSet *_discardedTranslation;
}

-(void)setupCollisionHandlers {
    
    __block CollisionResolver *engine = self;
    
    _collisionHandlers[[self collHdlrKey:@"bullet" and:@"bullet"]] =
    ^(SimulationObject *bullet1, SimulationObject *bullet2){
        [[engine discardedObjectsIds] addObject:bullet2.name];
    };
    
    _collisionHandlers[[self collHdlrKey:@"bullet" and:@"warrior"]] =
    ^(SimulationObject *bullet, SimulationObject *warrior){
        ((WGS *)warrior.gameSt).hitPoints -= 1;
    };
    
    _collisionHandlers[[self collHdlrKey:@"bulletFraction" and:@"warrior"]] =
    ^(SimulationObject *bullet, SimulationObject *warrior){
        ((WGS *)warrior.gameSt).hitPoints -= 1;
    };
    
    _collisionHandlers[[self collHdlrKey:@"knife" and:@"warrior"]] =
    ^(SimulationObject *knife, SimulationObject *warrior){
        ((WGS *)warrior.gameSt).hitPoints -= 5;
    };

    _collisionHandlers[[self collHdlrKey:@"bullet" and:@"*"]] =
    ^(SimulationObject *bullet, SimulationObject *smth){
        [[engine discardedObjectsIds] addObject:bullet.name];
    };
    
    _collisionHandlers[[self collHdlrKey:@"bulletFraction" and:@"*"]] =
    ^(SimulationObject *bullet, SimulationObject *smth){
        if ([smth.type isEqualToString:@"bulletFraction"]) { return; }
        [[engine discardedObjectsIds] addObject:bullet.name];
    };

    _collisionHandlers[[self collHdlrKey:@"knife" and:@"*"]] =
    ^(SimulationObject *knife, SimulationObject *smth){
        if (((Knife *)knife.gameSt).thrown) {
            [[engine discardedObjectsIds] addObject:knife.name];
        }
    };
    
    
    //TODO: remove block duplication
    _collisionHandlers[[self collHdlrKey:@"static_map" and:@"*"]] =
    ^(SimulationObject *map, SimulationObject *smth){
        [CollisionResolver resolvePenetrationForObj:map andObj:smth];

        [[CollisionResolver discardedTranslation] addObject:smth.name];
    };
    
    _collisionHandlers[[self collHdlrKey:@"warrior" and:@"warrior"]] =
    ^(SimulationObject *warrior1, SimulationObject *warrior2) {
        [CollisionResolver resolvePenetrationForObj:warrior1 andObj:warrior2];
    };
}

+(CGRect)boundingBoxToRect:(CollisionBox *)bb {
    return CGRectMake(bb.minX, bb.minY, bb.maxX - bb.minX, bb.maxY - bb.minY);
}

+(void)resolvePenetrationForObj:(SimulationObject *)obj1
                         andObj:(SimulationObject *)obj2 {

    float minDelta = INFINITY;
    GLKVector3 result = GLKVector3Make(0, 0, 0);
    GLKVector3 signs1 = GLKVector3Make(0, 0, 0);
    GLKVector3 signs2 = GLKVector3Make(0, 0, 0);
    
    //determine overall displacement dimension by entire BB
    CGRect entireObj1Rect = [self boundingBoxToRect:obj1.physicsSt.physObject.boundingBox];
    CGRect entireObj2Rect = [self boundingBoxToRect:obj2.physicsSt.physObject.boundingBox];
    CGRect entireIntersectionRect = CGRectIntersection(entireObj1Rect, entireObj2Rect);
    if (CGRectEqualToRect(entireIntersectionRect, CGRectNull)) {
        NSLog(@"BUG: False penetraction resolution request");
    }
    
    for (PhysicsObject *o1Part in [obj1.physicsSt.physObject relatedParts]) {
        for (PhysicsObject *o2Part in [obj2.physicsSt.physObject relatedParts]) {

            //TODO: make more precise (OOBB e.g.) and incorparate compute request to
  //  PhysicsObject *o1Part = obj1.physicsSt.physObject;
  //  PhysicsObject *o2Part = obj2.physicsSt.physObject;
    
            CGRect obj1Rect = [self boundingBoxToRect:o1Part.boundingBox];
            CGRect obj2Rect = [self boundingBoxToRect:o2Part.boundingBox];
            
            CGRect intersectionRect = CGRectIntersection(obj1Rect, obj2Rect);
            if (CGRectEqualToRect(intersectionRect, CGRectNull)) {
                continue;
            }
            
            if (intersectionRect.size.height > intersectionRect.size.width) {
                if (minDelta > intersectionRect.size.width && intersectionRect.size.width > FLT_EPSILON) {
                    minDelta = result.x = intersectionRect.size.width + FLT_EPSILON;
                    result.y = 0;
                    
                    signs1.x = fabs(obj1Rect.origin.x - intersectionRect.origin.x) < FLT_EPSILON ? 1 : -1;
                    signs2.x = fabs(obj2Rect.origin.x - intersectionRect.origin.x) < FLT_EPSILON ? 1 : -1;
                }
            } else {
                if (minDelta > intersectionRect.size.height && intersectionRect.size.height > FLT_EPSILON) {
                    minDelta = result.y = +intersectionRect.size.height + FLT_EPSILON;
                    result.x = 0;
                    
                    signs1.y = fabs(obj1Rect.origin.y - intersectionRect.origin.y) < FLT_EPSILON ? 1 : -1;
                    signs2.y = fabs(obj2Rect.origin.y - intersectionRect.origin.y) < FLT_EPSILON ? 1 : -1;
                }
            }

  
        }
    }
    
    if (minDelta == INFINITY) {
        NSLog(@"Unable to find penetraction");
        //Reaching this code means bug somewhere in CD.
        //The best we can do is just roll back transmission
        obj2.desiredWorldTransformMtx = obj2.worldTransformMtx;
        return;
    }
    

    GLKVector3 restVelocity = GLKVector3Make(0, 0, 0);
    bool obj1IsStatic = GLKVector3AllEqualToVector3(restVelocity, obj1.physicsSt.currVelocity);
    bool obj2IsStatic = GLKVector3AllEqualToVector3(restVelocity, obj2.physicsSt.currVelocity);
    
    if (!obj1IsStatic && !obj2IsStatic) {
        result = GLKVector3MultiplyScalar(result, 2);
    }
    
    if (!obj1IsStatic) {
        GLKVector3 obj1disp = GLKVector3Multiply(result, signs1);
        obj1.desiredWorldTransformMtx = GLKMatrix4TranslateWithVector3(obj1.desiredWorldTransformMtx, obj1disp);
    }

    if (!obj2IsStatic) {
        GLKVector3 obj2disp = GLKVector3Multiply(result, signs2);
        obj2.desiredWorldTransformMtx = GLKMatrix4TranslateWithVector3(obj2.desiredWorldTransformMtx, obj2disp);
    }
}


-(NSString *)collHdlrKey:(NSString *)type1
                     and:(NSString *)type2 {
    return [NSString stringWithFormat:@"%@-%@", type1, type2];
}

-(NSString *)collHdlrKeyObj:(SimulationObject *)obj1
                     andObj:(SimulationObject *)obj2 {
    return [self collHdlrKey:obj1.type and:obj2.type];
}

-(id)init {
    self = [super init];
    if (self) {
        _discardedObjIds = [NSMutableArray new];
        _collisionHandlers = [NSMutableDictionary new];
        _discardedTranslation = [NSMutableSet new];
        [self setupCollisionHandlers];
    }
    return self;
}


-(NSMutableArray *)discardedObjectsIds { return _discardedObjIds; }
-(NSMutableSet *)discardedTranslation { return _discardedTranslation; }
-(NSMutableDictionary *)collisionHandlers { return _collisionHandlers; }

+(NSMutableArray *)discardedObjectsIds {
    return [[self instance] discardedObjectsIds];
}

+(NSMutableSet *)discardedTranslation {
    return [[self instance] discardedTranslation];
}

+(CollisionResolver *)instance {
    static CollisionResolver *inst;
    if (!inst) {
        inst = [CollisionResolver new];
    }
    return inst;
}

+(bool)execCollisionHandlerWithParam1:(NSString *)param1 param2:(NSString *)param2
    obj1:(SimulationObject *)obj1 obj2:(SimulationObject *) obj2{
    
    CollisionResolver *engine = [self instance];
    CollisionHandler commonHandler = [
        [engine collisionHandlers] objectForKey:
            [engine collHdlrKey:param1 and:param2]
    ];
    
    if (commonHandler) { commonHandler(obj1, obj2); }
    return commonHandler != nil;
}


+(void)resolveCollisionOfObject:(NSString *)obj1Id
                     withObject:(NSString *)obj2Id {
    SimulationObject *obj1 = [SimObjFactory createdObjectById:obj1Id];
    SimulationObject *obj2 = [SimObjFactory createdObjectById:obj2Id];
    
    [self execCollisionHandlerWithParam1:obj1.type param2:@"*"
                                    obj1:obj1 obj2:obj2];
    [self execCollisionHandlerWithParam1:obj2.type param2:@"*"
                                    obj1:obj2 obj2:obj1];
    
    bool handlerFound = [self execCollisionHandlerWithParam1:obj1.type
        param2:obj2.type obj1:obj1 obj2:obj2];
    if (!handlerFound) {
        [self execCollisionHandlerWithParam1:obj2.type
            param2:obj1.type obj1:obj2 obj2:obj1];
    }
    
}

+(void)prepareForCheck {
    [[self discardedTranslation] removeAllObjects];
    [[self discardedObjectsIds] removeAllObjects];
}

@end
