//
//  Animation.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/23/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol KeyFrame <NSObject>

@required
-(id)createCopy;
-(BOOL)step:(float)elapsedTime;
-(void)setTargetId:(NSString *)targetId;

@end
