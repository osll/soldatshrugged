//
//  Animation.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/23/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "SkeletonKeyFrame.h"
#import "InputManager.h"
#import "MoveCommand.h"
#import "SimObjFactory.h"
#import "SkeletonMovementCommand.h"

@implementation SkeletonKeyFrame {
    NSDictionary *_jointRotationCfg;
    float _duration;
    NSString *_simObjId;
    
    SkeletonPhysicsObject *_currObj;
    float _elapsedTime;
    GLKVector3 _transpositionToGo;
    GLKVector3 _transpositionCfg;
    
    BOOL _ignoreChecks;
}

-(id)initWithDuration:(float)duration
        jointRotation:(NSDictionary *)jointRotations {
    return [self initWithDuration:duration jointRotation:jointRotations transposition:GLKVector3Make(0, 0, 0) ignoreChecks:NO];
}

-(id)initWithDuration:(float)duration
        jointRotation:(NSDictionary *)jointRotations
        transposition:(GLKVector3)transposition {
    return [self initWithDuration:duration jointRotation:jointRotations transposition:transposition ignoreChecks:NO];
}

-(id)initWithDuration:(float)duration
        jointRotation:(NSDictionary *)jointRotations
        transposition:(GLKVector3)transposition
        ignoreChecks:(BOOL)ignoreChecks{
    self = [super init];
    if (self) {
        _duration = duration;
        _jointRotationCfg = jointRotations;
        _transpositionCfg = _transpositionToGo = transposition;
        _ignoreChecks = ignoreChecks;
    }
    return self;
}

-(void)setTargetId:(NSString *)targetId {
    PhysicsObject *po = [SimObjFactory createdObjectById:targetId].physicsSt.physObject;
    _currObj = (SkeletonPhysicsObject *)po;
    _simObjId = targetId;
}

-(id)createCopy {
    return [[SkeletonKeyFrame alloc] initWithDuration:_duration jointRotation:_jointRotationCfg transposition:_transpositionCfg];
}

-(NSMutableDictionary *)computeJointRotationsWithElapsedTime:(float)elapsedTime
                                                 andTimeToGo:(float)timeToGo {
    NSMutableDictionary *jointRotCmds = [NSMutableDictionary new];
    for (NSString *jointName in _jointRotationCfg) {
        Joint *joint = [_currObj jointById:jointName];
        float jointRotation = joint.rotationAngle;
        float desiredRotation = [_jointRotationCfg[jointName] floatValue];
        
        float requiredAngle = (desiredRotation - jointRotation)/timeToGo * elapsedTime;
        jointRotCmds[jointName] = @(requiredAngle);
    }
    return jointRotCmds;
}

-(BOOL)step:(float)elapsedTime {    
    float timeToGo = _duration - _elapsedTime;
    if (timeToGo <= -FLT_EPSILON) {
        //ensure joint in end position
        SkeletonMovementCommand *skCmd = [[SkeletonMovementCommand alloc] initWithActorName:_simObjId commands:[self computeJointRotationsWithElapsedTime:elapsedTime andTimeToGo:elapsedTime]];
        [InputManager submitCommand:skCmd];
        return YES;
    }
    _elapsedTime += elapsedTime;
    
    
    
    GLKVector3 transpositionDelta = GLKVector3MultiplyScalar(_transpositionToGo, elapsedTime / timeToGo);
    _transpositionToGo = GLKVector3Subtract(_transpositionToGo, transpositionDelta);
    
    SkeletonMovementCommand *skCmd = [[SkeletonMovementCommand alloc] initWithActorName:_simObjId commands:[self computeJointRotationsWithElapsedTime:elapsedTime andTimeToGo:timeToGo]];
    skCmd.ignoreCollisionCheck = _ignoreChecks;
    
    [InputManager submitCommand:skCmd];

    [InputManager submitCommand:[[MoveCommand alloc] initWithActorName:_simObjId translation:transpositionDelta]];
    
    return NO;
}

@end
