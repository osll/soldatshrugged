//
//  Knife.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 9/5/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "Weapon.h"
#import "GameManager.h"

@interface Knife : GameStrategy<Weapon>

@property (assign, nonatomic) BOOL thrown;

@end
