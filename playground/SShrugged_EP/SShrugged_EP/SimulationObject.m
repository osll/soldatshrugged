//
//  WorldObject.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/17/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "SimulationObject.h"
#import "DrawingCtx.h"
#import "SkeletonPhysicsObject.h"

@implementation SimulationObject {
    NSDictionary *_particleTypeEmitterCorr;
}

-(void)setGameSt:(GameStrategy *)gameSt {
    _gameSt = gameSt;
    _gameSt.name = self.name;
    _gameSt.type = self.type;
}

-(void)setWorldTransformMtx:(GLKMatrix4)transform {
    _worldTransformMtx = transform;
    
    self.desiredWorldTransformMtx = transform;
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@Transform", _name] object:self];
}

-(void)setDesiredWorldTransformMtx:(GLKMatrix4)transform {
    if (memcmp(transform.m, _desiredWorldTransformMtx.m, sizeof(GLKMatrix4)) == 0) {
        return;
    }
    
    _desiredWorldTransformMtx = transform;
    
    //propagate to strategies
    _physicsSt.physObject.transposition = transform;
}

-(void)setParticleEmitterCorr:(NSDictionary *)corr {
    _particleTypeEmitterCorr = corr;
}

//designated
- (id)initWithId:(NSString *)name type:(NSString *)type{
    self = [super init];
    if (self) {
        _name = name;
        _type = type;
        _worldTransformMtx = GLKMatrix4Identity;
        _desiredWorldTransformMtx = GLKMatrix4Identity;
        _particleTypeEmitterCorr = [NSDictionary new];
        _displaySt = [[DisplayStrategy alloc] initWithSimObjectName:name];
        _physicsSt = [[PhysicsStrategy alloc] initWithSimObjectName:name physObject:[PhysicsObject new]];
    }
    return self;
}

- (void)draw {
    [DrawingCtx instance].currDrawObjectId = self.name;
    [self.displaySt draw];
}

-(BOOL)setupParticlePhysicsParams:(SimulationObject *)particle {
    NSString *emitterId = _particleTypeEmitterCorr[particle.type];
    if (!emitterId) {
        NSLog(@"Can't init particle of type %@ with emitter of type %@",
              particle.type, self.type);
        return NO;
    }


    //FIXME: get emitter object
    PhysicsObject *emitterPhysObject = [(SkeletonPhysicsObject *)self.physicsSt.physObject physicsPartByName:emitterId];
    // To get init position
    // 1) orientate like emitter
    // 2) translate to emit point
    
    particle.worldTransformMtx = GLKMatrix4TranslateWithVector3(emitterPhysObject.currentTransposition, emitterPhysObject.particleEmitPoint);
    //TODO:[move to cfg] init emission has direction (-1,0,0);
    particle.physicsSt.currVelocity = GLKMatrix4MultiplyVector3(emitterPhysObject.currentTransposition,
                                                                particle.physicsSt.currVelocity);


    return YES;
}

-(BOOL)isMovementInProgress {
    if ([_physicsSt.physObject isMemberOfClass:[SkeletonPhysicsObject class]]) {
        return YES; // TODO: determine joint rotation
    }
    
    return memcmp(self.desiredWorldTransformMtx.m, self.worldTransformMtx.m,
                        sizeof(self.desiredWorldTransformMtx.m)) != 0;
}

#pragma mark - World Transformation
-(void) commitWorldTransform {
    self.worldTransformMtx = self.desiredWorldTransformMtx;
}
-(void) rollbackWorldTransform {
    self.desiredWorldTransformMtx = self.worldTransformMtx;
}


@end
