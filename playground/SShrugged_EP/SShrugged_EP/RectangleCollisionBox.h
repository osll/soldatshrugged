//
//  RectangleCollisionBox.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/27/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CollisionBox.h"

static const GLKVector4 RCB_zeroVector4 = {0.0f, 0.0f, 0.0f, 1.0f};

@interface RectangleCollisionBox : CollisionBox

@property (assign, nonatomic, readonly) CGRect bounds;

-(id)initWithWidth:(float)width andHeight:(float)height;
-(id)initWithBounds:(CGRect)bounds;

//TODO: move internal methods to separate categoro
+(BOOL)isOverlapsP1S:(float)start1 andP1E:(float)end1
             withP2S:(float)start2 andP2E:(float)end2;

-(BOOL)doesCollideWithRectBox:(RectangleCollisionBox *)box;
+(GLKVector4)boxOriginWorldPosition:(RectangleCollisionBox *)box;
-(CGRect)roughBoundingRect;

@end
