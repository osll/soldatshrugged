//
//  DrawingCtx.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/18/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DrawingCtx : NSObject
@property (strong, nonatomic) NSString *currDrawObjectId;

- (id)init;
+ (DrawingCtx *)instance;
@end
