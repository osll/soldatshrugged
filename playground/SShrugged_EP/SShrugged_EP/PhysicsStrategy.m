//
//  PhyciscStrategy.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/19/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "PhysicsStrategy.h"
#import "SimObjFactory.h"
#import "PhysicsManager.h"

@implementation PhysicsStrategy {
    GLKVector3 _emptyVec3;
    
}

-(id)initWithSimObjectName:(NSString *)simObjName
                physObject:(PhysicsObject *)physObj; {
    self = [super init];
    if (self) {
        _simObjId = simObjName;
        _physObject = physObj;
        _currVelocity = _currAcceleration = _emptyVec3 = GLKVector3Make(0, 0, 0);
    }
    return self;
}

-(void)updateVelocity:(CFTimeInterval)timeElapsed {
#ifdef DEBUG
    timeElapsed = 0.02;
#endif
    _currVelocity = GLKVector3Add(_currVelocity, GLKVector3MultiplyScalar(_currAcceleration, timeElapsed));
}


-(void)updateTmpObjectPositionWithMvmnt:(GLKVector3)mvmnt {
    _ttl -= 1;
    SimulationObject *obj = [SimObjFactory createdObjectById:_simObjId];
    
    obj.desiredWorldTransformMtx =
    GLKMatrix4Multiply(
        GLKMatrix4MakeTranslation(mvmnt.x, mvmnt.y, mvmnt.z),
        obj.desiredWorldTransformMtx);
}


@end
