//
//  TextureDrawEffect.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/28/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "OGLEffect.h"


@interface TextureDrawEffect : OGLEffect

@property (assign, nonatomic) float rowShift;

-(instancetype)initWithImageName:(NSString *)imageName;

@end
