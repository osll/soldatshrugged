//
//  HumanSkeletonConstants.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 9/2/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>

//human parts abbreviations:

//body
extern NSString * const hsNeckId; // torso - head
extern NSString * const hsLoinId; // hips - torso

//arms
extern NSString * const hsRightShoulderId; //torso - right hand high
extern NSString * const hsRightElbowId; //right hand high - right hand low
extern NSString * const hsRightWristId; //right hand low - weapon

extern NSString * const hsLeftShoulderId; //torso - left hand high
extern NSString * const hsLeftElbowId; //left hand high - left hand low

//legs
extern NSString * const hsRightHipId;  // hips - right leg high
extern NSString * const hsRightKneeId; // right leg high - right leg low
extern NSString * const hsRightAnkleId; // right leg low - right shoe

extern NSString * const hsLeftHipId;  // hips - left leg high
extern NSString * const hsLeftKneeId; // left leg high - lefti leg low
extern NSString * const hsLeftAnkleId; //

@interface HumanSkeletonConstants : NSObject

@end
