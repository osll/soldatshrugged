//
//  Shotgun.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 9/6/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "Shotgun.h"
#import "SimObjFactory.h"
#import "BonePhysicsObject.h"
#import "HumanSkeletonConstants.h"
#import "Animation.h"
#import "SkeletonKeyFrame.h"
#import "WorldCamera.h"

@implementation Shotgun {
    NSString * _owner;
}


-(float)fireRate { return 2; }

-(NSArray *)createBulletWithId:(NSString *)bId; {
    NSMutableArray *bullets = [NSMutableArray new];
    for (int i = 1; i <= 6; i++) {
        NSString *bfr =[NSString stringWithFormat:@"%@-%d",bId,i];
        SimulationObject *bullet = [SimObjFactory createObjectOfType:@"bulletFraction"
                                                               withId:bfr];
        bullet.physicsSt.currVelocity = GLKVector3Make(-0.1-0.5*rand()/RAND_MAX, 0.05 - 0.1*rand()/RAND_MAX, 0);
        bullet.physicsSt.ttl = 1000;
        [bullets addObject:bullet];
        [bullet.physicsSt.physObject.ignoredCollisionsWithObjs addObject:_owner];
        [bullet.physicsSt.physObject.ignoredCollisionsWithObjs addObject:self.name];
    }
    
    return bullets;
}

-(void)assignToWarrior:(SkeletonPhysicsObject *)sltn {
    _owner = sltn.name;
    BonePhysicsObject *wbpo = (BonePhysicsObject *)[SimObjFactory createdObjectById:self.name].physicsSt.physObject;
    BonePhysicsObject *rArmBpo = (BonePhysicsObject *)[sltn physicsPartByName:@"RightArmLo"];
    
    Joint *rightWristJoint = [[Joint alloc] initWithName:hsRightWristId
                                              parentBone:rArmBpo.bone childBone:wbpo.bone
                                               connPoint:GLKVector3Make(0.10, 0.0, 0.0)];
    
    [sltn addJoint:rightWristJoint];
    [rArmBpo.bone.childJoints addObject:rightWristJoint];
    [rightWristJoint rotateChildOverJoint:-M_PI_2];
    [rightWristJoint setRotationConstrMinAng:-M_PI_2-M_PI/30  maxRotAngle:-M_PI_2 + M_PI/30 isFwrd:YES];
    [sltn addPart:wbpo];
    
}

-(NSDictionary *)weaponRelatedAnimations {
    return @{
             @"WarriorAimOn":[[Animation alloc] initWithKeyFrames:@[]],
             @"WarriorAimOff":[[Animation alloc] initWithKeyFrames:@[[[SkeletonKeyFrame alloc] initWithDuration:0.2 jointRotation:@{hsRightShoulderId: @(0), hsRightElbowId: @(M_PI_2), hsLeftShoulderId:@(-M_PI/5), hsLeftElbowId:@(-M_PI/5)} transposition:GLKVector3Make(0, 0.0, 0)]]],
             @"WeaponRecoilAiming":[[Animation alloc] initWithKeyFrames:@[]],
             @"WeaponRecoil":[[Animation alloc] initWithKeyFrames:@[]]
             };
}

-(void)postprocessShoot:(GLKVector3)shootDirection {
    [WorldCamera pullback:GLKVector3MultiplyScalar(shootDirection, -1)];
}

@end
