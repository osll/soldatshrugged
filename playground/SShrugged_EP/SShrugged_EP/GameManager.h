//
//  GameManager.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/20/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameManager : NSObject

+(void)setupWorld;
+(void)prepareForStart;

+(void)fire:(NSString *)emitterName;
+(void)jump:(NSString *)emitterName;
+(void)turnAround:(NSString *)objectId;
+(void)update;

-(float)playerFireRate;
+(float)slowDownFactor;

@end
