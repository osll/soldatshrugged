//
//  ColoredDrawEffect.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/18/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "OGLEffect.h"
#import "OGLBuffer.h"

@interface ColoredDrawEffect : OGLEffect

@property (assign, nonatomic) float opacity;

- (id)init;
- (id)initWithOpacity:(float)opacity;
- (void)bindAttributes:(id<OGLBuffer>)buffer;
- (void)configureUniforms:(GLKMatrix4)meshTransform;

@end
