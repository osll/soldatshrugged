//
//  WorldObject.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/17/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Mesh.h"
#import "PhysicsStrategy.h"
#import "DisplayStrategy.h"
#import "GameStrategy.h"
#import "GLKit/GLKit.h"

//represents object what is used in simulation (game)
@interface SimulationObject : NSObject

@property (nonatomic, strong, readonly) NSString* name;
@property (nonatomic, strong, readonly) NSString* type;


@property (nonatomic, strong) PhysicsStrategy *physicsSt;
@property (nonatomic, strong) DisplayStrategy *displaySt;
@property (nonatomic, strong) GameStrategy *gameSt;

//current object-to-world transform matrix
@property (nonatomic, assign, readonly) GLKMatrix4 worldTransformMtx;
//desired object-to-world transform matrix
@property (nonatomic, assign) GLKMatrix4 desiredWorldTransformMtx;

//TODO move to separate ``View'' component
//@property (nonatomic, assign) GLKMatrix4 camTransformMtx;

-(id)initWithId:(NSString*)name type:(NSString *)type;
-(void)draw;

-(void)setParticleEmitterCorr:(NSDictionary *)corr;
-(BOOL)setupParticlePhysicsParams:(SimulationObject *)particle;
-(BOOL)isMovementInProgress;


#pragma mark - World Transformation
-(void) commitWorldTransform;
-(void) rollbackWorldTransform;

@end
