//
//  PhyciscStrategy.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/19/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLKit/GLKit.h"
#import "PhysicsObject.h"

@interface PhysicsStrategy : NSObject

@property (strong, nonatomic) NSString* simObjId;
@property (strong, nonatomic) PhysicsObject *physObject;

@property (assign, nonatomic) GLKVector3 currVelocity;
@property (assign, nonatomic) GLKVector3 currAcceleration;

@property (assign, nonatomic) float ttl;

-(id)initWithSimObjectName:(NSString *)simObjName
              physObject:(PhysicsObject *)physObj;

-(void)updateTmpObjectPositionWithMvmnt:(GLKVector3)mvmnt;

-(void)updateVelocity:(CFTimeInterval)timeElapsed;

@end
