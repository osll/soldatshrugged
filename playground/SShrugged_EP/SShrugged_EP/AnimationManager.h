//
//  AnimationManager.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/23/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Animation.h"

@interface AnimationManager : NSObject

+(void)addAnimation:(Animation *)anim byKey:(NSString *)key withMerge:(BOOL)mergeEnabled;
+(void)addAnimationId:(NSString *)animId byKey:(NSString *)key forTarget:(NSString *)targetId;
+(void)perfromAnimations:(float)elapsedTime;

+(void)registerAnimations:(NSDictionary *)anims;

@end
