//
//  CollisionBox.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/19/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLKit/GLKit.h"
#import "TranslationProvider.h"
#import "ParticleEmitter.h"

@interface CollisionBox : NSObject

@property (assign, nonatomic) GLKMatrix4 selfTransposition;
@property (weak, nonatomic) id<TranspositionProvider> transpositionProvider;

-(BOOL)checkCollision:(CollisionBox *)actor;
-(id)initWithTranslation:(GLKMatrix4)initTranslation;
-(BOOL)checkIfPosInside:(GLKVector4)pos;
-(float)maxX;
-(float)minX;
-(float)maxY;
-(float)minY;
-(GLKMatrix4)boxTransposition;

/* Should be called before each series of checks*/
-(void)updateBoxTransposition;


@end
