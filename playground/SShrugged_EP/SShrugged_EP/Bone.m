//
//  Bone.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/21/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "Bone.h"
#import "Joint.h"
#import "SimObjFactory.h"

@implementation Bone

-(id)init {
    self = [super init];
    if (self) {
        _childJoints = [NSMutableSet new];
        _selfTransposition = _externalTransposition = GLKMatrix4Identity;
    }
    return self;
}

-(GLKMatrix4)compoundTranslation {
    return GLKMatrix4Multiply(_externalTransposition, _selfTransposition);
}

-(void)propogateTranspositionToChildren {
    GLKMatrix4 transposition = [self compoundTranslation];
    for (Joint *joint in self.childJoints) {
        [joint propogateTransposition:transposition];
    }
}

-(void)setSelfTransposition:(GLKMatrix4)selfTransposition {
    _selfTransposition = selfTransposition;
    [self propogateTranspositionToChildren];
}

-(void)setExternalTransposition:(GLKMatrix4)externalTransposition {
    _externalTransposition = externalTransposition;
    [self propogateTranspositionToChildren];
}

@end
