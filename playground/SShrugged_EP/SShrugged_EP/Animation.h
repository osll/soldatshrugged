//
//  Animation.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/23/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Animation : NSObject

@property (assign, nonatomic) NSString *targetId;

-(id)createCopy;
-(instancetype)initWithKeyFrames:(NSArray *)keyFrames;
-(BOOL)step:(float)elapsedTime;
-(void)merge:(Animation *)animation;

@end
