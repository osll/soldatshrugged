//
//  PhysicsManager.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 7/19/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SimulationObject.h"
#import "GLKit/GLKit.h"

@interface PhysicsManager : NSObject

+(float)timeDelta;

+(GLKVector3)gravityAcceleration;

+(BOOL)checkCollisionsForObject:(NSString *)objName;
+(void)updatePhysics;
+(void)addAlwaysMovingObject:(NSString *)objName;
+(void)addFirmObject:(SimulationObject *)objName;

+(void)forgetObject:(NSString *)objName;
+(PhysicsObject *)physicsObjectById:(NSString *)objName;

+(SimulationObject *)lookupFirmObjectByLocation:(GLKVector4)pos;


@end
