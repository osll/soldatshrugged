//
//  SkeletonMovementCommand.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/22/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Command.h"

@interface SkeletonMovementCommand : NSObject<Command>

@property (assign, nonatomic) BOOL ignoreCollisionCheck;

-(id)initWithActorName:(NSString *)actorName
              commands:(NSDictionary *)skeletonTranslation;

@end
