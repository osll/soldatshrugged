//
//  PDController.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 9/6/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "PDController.h"

@implementation PDController {
    float _damping;
    float _springConst;
    
    GLKVector3 _velocity;
}

-(id)initWithCurrentPosition:(GLKVector3)pos
                      daming:(float)damping
                 springConst:(float)springConst {
    self = [super init];
    if (self) {
        _damping = damping;
        _springConst = springConst;
        _currentValue = pos;
        _velocity = GLKVector3Make(0, 0, 0);
    }
    return self;
}

-(void)update:(float)timeDelta {
    GLKVector3 error = GLKVector3Subtract(_currentValue, _desiredValue);
    GLKVector3 acc = GLKVector3Add(
        GLKVector3MultiplyScalar(error, -_springConst),
        GLKVector3MultiplyScalar(_velocity, -_damping)
    );
    
    _velocity = GLKVector3Add(_velocity, GLKVector3MultiplyScalar(acc, timeDelta));
    _currentValue = GLKVector3Add(_currentValue, GLKVector3MultiplyScalar(_velocity, timeDelta));
}

@end
