//
//  Pistol.m
//  SShrugged_EP
//
//  Created by Hatless Fox on 9/4/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import "Pistol.h"
#import "SimObjFactory.h"
#import "BonePhysicsObject.h"
#import "HumanSkeletonConstants.h"
#import "Animation.h"
#import "SkeletonKeyFrame.h"

@implementation Pistol {
    NSString *_owner;
}

-(float)fireRate { return 0.5; }

-(NSArray *)createBulletWithId:(NSString *)bId; {
    SimulationObject *bullet = [SimObjFactory createObjectOfType:@"bullet"
                                                          withId:bId];
    bullet.physicsSt.physObject.name = bId;
    bullet.physicsSt.currVelocity = GLKVector3Make(-0.2, 0.0, 0);
    bullet.physicsSt.ttl = 1000;
    [bullet.physicsSt.physObject.ignoredCollisionsWithObjs addObject:_owner];

    return @[bullet];
}

-(void)assignToWarrior:(SkeletonPhysicsObject *)sltn {
    _owner = sltn.name;
    
    BonePhysicsObject *wbpo = (BonePhysicsObject *)[SimObjFactory createdObjectById:self.name].physicsSt.physObject;
    BonePhysicsObject *rArmBpo = (BonePhysicsObject *)[sltn physicsPartByName:@"RightArmLo"];
    
     Joint *rightWristJoint = [[Joint alloc] initWithName:hsRightWristId
                                            parentBone:rArmBpo.bone childBone:wbpo.bone
                                            connPoint:GLKVector3Make(0.10, 0.0, 0.0)];
    
    [sltn addJoint:rightWristJoint];
    [rArmBpo.bone.childJoints addObject:rightWristJoint];
    [rightWristJoint rotateChildOverJoint:-M_PI_2];
    [rightWristJoint setRotationConstrMinAng:-M_PI_2-M_PI/30  maxRotAngle:-M_PI_2 + M_PI/30 isFwrd:YES];
    [sltn addPart:wbpo];

}

-(NSDictionary *)weaponRelatedAnimations {
    return @{
      @"WarriorAimOn":[[Animation alloc] initWithKeyFrames:
        @[[[SkeletonKeyFrame alloc] initWithDuration:0.2 jointRotation:
          @{hsRightShoulderId: @(-M_PI_2), hsRightElbowId: @(M_PI),
            hsRightWristId:@(-M_PI_2), hsLeftShoulderId:@(-M_PI_2), hsLeftElbowId:@(0)}
          transposition:GLKVector3Make(0, 0.0, 0)]]],
      @"WarriorAimOff":[[Animation alloc] initWithKeyFrames:
        @[[[SkeletonKeyFrame alloc] initWithDuration:0.2 jointRotation:
           @{hsRightShoulderId: @(0), hsRightElbowId: @(M_PI_2), hsLeftShoulderId:@(0), hsLeftElbowId:@(0)}
        transposition:GLKVector3Make(0, 0.0, 0)]]],
      
      
      @"WeaponRecoilAiming":[[Animation alloc] initWithKeyFrames:
        @[[[SkeletonKeyFrame alloc] initWithDuration:0.3 jointRotation:
           @{hsRightElbowId: @(M_PI_2), hsLeftElbowId: @(-M_PI_2)}],
          [[SkeletonKeyFrame alloc] initWithDuration:0.3 jointRotation:
           @{hsRightElbowId: @(M_PI), hsLeftElbowId: @(0)}]
        ]
      ],
      
      @"WeaponRecoil":[[Animation alloc] initWithKeyFrames:@[]]
    };
}

-(void)postprocessShoot:(GLKVector3)shootDirection {
    
}

@end
