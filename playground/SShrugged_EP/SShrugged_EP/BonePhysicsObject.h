//
//  PhysicsObjectBoned.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/21/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhysicsObject.h"

@interface BonePhysicsObject : PhysicsObject

@property (strong, nonatomic) Bone *bone;


@end
