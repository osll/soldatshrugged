//
//  PhysicsObject.h
//  SShrugged_EP
//
//  Created by Hatless Fox on 8/4/13.
//  Copyright (c) 2013 WooHoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CollisionBox.h"
#import "Bone.h"
#import "Joint.h"

@interface PhysicsObject : NSObject<TranspositionProvider, ParticleEmitter>

@property (strong, nonatomic) NSString *name;
//bounding box with cheap collision check
@property (strong, nonatomic) CollisionBox *boundingBox;
@property (assign, nonatomic) GLKVector3 particleEmitPoint;
@property (strong, nonatomic) NSMutableSet *ignoredCollisionsWithObjs;


//required translation
-(void)setTransposition:(GLKMatrix4)transposition;
-(NSArray*) relatedParts;
-(PhysicsObject *)controllerPart;

-(id)init;
-(id)initWithName:(NSString *)name;
-(BOOL)checkIfPosInside:(GLKVector4)pos;
-(BOOL)doesCollideWith:(PhysicsObject *)otherPhysObject;

-(void)prepareForCollisionDetection;

@end
